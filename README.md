# The Rust Programming Language

Notes from reading "the book".

## 01. getting started
### installation
### hello world
### hello cargo

verbesserung
## 02. guessing game tutorial


## 03. common programming concepts
### variables and mutability
### data types
### how functions work
### comments
### control flow


## 04. understanding ownership
### what is ownership
### references and borrowing
### slices


## 05. structs
### defining structs
### example structs
### method syntax


## 06. enums
### defining an enum
### match
### if let


## 07. packages crates and modules
### packages and crates for making libraries and executables
### modules and use to control scope and privacy


## 08. common collections
### vectors
### strings
### hash maps


## 09. error handling
### unrecoverable errors with panic
### recoverable errors with result
### to panic or not to panic


## 10. generics
### syntax
### traits
### lifetime syntax


## 11. testing
### writing tests
### running tests
### test organization


## 12. an io project
### accepting command line arguments
### reading a file
### improving error handling and modularity
### testing the librarys functionality
### working with environment variables
### writing to stderr instead of stdout


## 13. Functional Language Features

### Closures

Similar to lambda functions, can be assigned to variables and passed as arguments.
Syntax compared to normal function:

```rust
fn  add_one_v1   (x: u32) -> u32 { x + 1 }
let add_one_v2 = |x: u32| -> u32 { x + 1 };
let add_one_v3 = |x|             { x + 1 };
let add_one_v4 = |x|               x + 1  ;
```

Closures can use ("capture") variables of the environment their defined in.
These variables will need the `Copy` trait.

By adding the `move` keyword before the arguments, the closure can be forced to
take ownership of a environment variable instead of borrowing it.


### Iterators

The `Iterator` trait looks like the following:

```rust
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;

    // methods with default implementations elided
}
```

Implementations only need to implement the `next` method, which returns `None`
once the iterator is exploited.

A bunch of default methods are defined by `Iterator`:

```rust
let v1: Vec<i32> = vec![1, 2, 3];
// consuming methods
v1.iter().sum();
v1.iter().collect();
// iterator adaptors - return modified iterator (lazy!)
v1.iter().map(|x| x + 1);
v1.iter().filter(|x| x > 5);
v1.iter().zip(other_iterator);
v1.iter().skip(1);
// can be combined with consuming methods:
let v2: Vec<_> = v1.iter().map(|x| x + 1).collect();
```

Vectors can be turned into iterators in three different ways:

```rust
v1.iter(); // iterate over references
v1.iter_mut(); // iterate over mutable references
v1.into_iter(); // iterate over owned values
```


## 14. More About Cargo

### Release Profiles

In `Cargo.toml`, options can be defined for different release profiles.
E.g.:

```toml
[profile.dev]
opt-level = 0

[profile.release]
opt-level = 3
```

### Documentation Conventions

Documentation comments start with three slashes (`///`):

```rust
/// Adds one to the number given.
///
/// # Examples
///
/// \`\`\`
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// \`\`\`
pub fn add_one(x: i32) -> i32 {
    x + 1
    }
```

Common subsections are:
- Examples
- Panics
- Errors
- Safety

The documentation can be compiled to HTML with `cargo doc`.
`cargo test` will also run the code examples given as tests.

For documentation *inside* of the described object, `//!` is used.
This is typically done in the beginning of `src/lib.rs` or of modules.

### Publishing to crates.io

--- partially skipped ---

### Cargo Workspaces

--- skipped ---

### Installing Binaries

Crates with binary targets on crates.io can be installed with `cargo install <name>`.

### Extending Cargo

Binaries in the `$PATH` with names `cargo-something` can be run as cargo subcommands.
Available commands can be listed with `cargo --list`.


## 15. Smart Pointers

The standard pointer of Rust is the reference as indicated by `&`, but smart
pointer exist in the standard library, which can own the data instead of
borrowing it (e.g. `Vec<T>` and `String` are smart pointers).

Smart pointers implement the `Deref` and `Drop` traits.

### Box
A `Box` is used to store data on the heap instead of on the stack.
They have minimal overhead, as they just store a pointer with the type
information.

```rust
let b = Box::new(5);
println!("b = {}", b);
```

### Deref

The `Deref` trait allows to use the derefference operator `*` like a reference
would:

```rust
let x = 5;
let y = &x;
let z = Box::new(x);
assert_eq!(5, x);
assert_eq!(5, *y);
assert_eq!(5, *z);
```

A minimal struct having the `Deref` trait can be implemented like this:

```rust
use std::ops::Deref;

struct MyBox<T>(T);

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}
```

Internally Rust automatically calls `*(y.deref())`, instead of `*y`, when `y`
has the `Deref` trait.
Also, to match function and method signatures, Rust will call `.deref()` as
many times as necessary (and possible).
The latter is known as *Deref Coersion*.

For mutable references, one needs to implement the `DerefMut` trait.

### Drop

The `Drop` trait implements a method of signature `fn drop(&mut self)`, that is
called whenever a value runs out of scope.

The `drop` method can not be called manually (to avoid a "double free" error.
To drop a value before it runs out of scope, pass it to `std::mem::drop` (no
need for `use`).

### Rc

`Rc<T>` is a reference counting smart pointer for single-threaded scenarios.

```rust
enum List {
    Cons(i32, Rc<List>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::rc::Rc;

fn main() {
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    let b = Cons(3, Rc::clone(&a));
    let c = Cons(4, Rc::clone(&a));
}
```

### Interior Mutability

The `RefCell<T>` type can be used in single-threaded scenarios to circumvent
some borrowing checks by the compiler, mistakes will cause a panic at runtime
instead.

The creation is similar to `Box<T>`.
References can be retrieved by the methods `borrow` and `borrow_mut`.

For example, the following struct can change its internal state in the method
`store`, even though it takes an immutable reference of `self` as the first
argument:

```rust
use std::cell::RefCell;

struct StoreString {
    strings: RefCell<Vec<String>>,
}

impl StoreString {
    fn new() -> StoreString {
        StoreString {
            strings: RefCell::new(vec![]),
        }
    }

    fn store(&self, string_to_store: &str) {
        self.strings.borrow_mut().push(String::from(string_to_store));
    }
}
```

It is also common to use `Rc<RefCell<T>>` to combine the benefits of both smart
pointer types.

### Reference Cycles

Using `RefCell`, it is possible to create cyclic reference loops, that will stay
in memory indefinitely and thus leak memory.

One way to reduce the risk of reference cycles is to use `Rc::downgrade` to get
a `Weak<T>` smart pointer, which does not influence the lifetime of the object.
By calling the `upgrade` method on a `Weak<T>` an `Option<Rc<T>>` can be
retrieved.


## 16. Concurrency

concurrency = parts execute independently
parallelism = parts execute at the same time

These are the same for the purpose of this chapter.

### Threads

The standard library implements only 1:1 threading (one Rust thread = one OS
thread), as opposed to *green threads* as provided by some other languages.
This leads to less "runtime" in the produced binaries.

To start a new thread, call `std::thread::spawn()` with a closure as argument:

```rust
use std::thread;
use std::time::Duration;

fn main() {
    thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }
}
```

The program will stop once the main thread is over, even if the spawned thread
is not.

`spawn` returns a `JoinHandle`.
Calling the `join` method on this will block the current thread until the tread
of the handle is finished.

### Message Passing

Threads can use *channels* to communicate with each other:

```rust
use std::sync::mpsc;
use std::thread;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let val = String::from("hi");
        tx.send(val).unwrap();
    });

    let received = rx.recv().unwrap();
    println!("Got: {}", received);
}
```

`mpsc` stands for *multiple producers, single consumer*.
Instead of `recv`, the receiving end can also call `try_recv`, which is
non-blocking.
The transmitter can be cloned, the receiver is also iterable.

### Shared State

To share data between threads, a `Mutex<T>` can be used.
It holds a smart pointer that can be obtained via the `lock` method:

```rust
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
```

The `Arc<T>` (atomic reference counting) pointer is needed since a simple
`Rc<T>` is not thread-safe and `counter` can not be moved into multiple
threads.

### Extensible Concurrency sync and send

Most concurrency features are implemented in the standard library, not in the
core language.
To develop one's own, the two `std::marker` traits can be used:

- `Send` indicates, that a ownership of the type can be transferred between
  threads
- `Sync` indicates, that it is safe to reference the type from multiple threads

Types that are only composed of `Send`/`Sync` types automatically have the same
trait.
*It is unsafe to implement these traits manually!*


## 17. Object Orient Programming

### What is Object Orientation

OOP languages have as common characteristics:
- objects (data + behaviour, as `struct` and `enum` have)
- encapsulation (as can be set with the `pub` keyword)
- inheritance (not possible in Rust)

Instead of inheritance, one can use Rust's `trait` feature and generic types in
many cases.

### Trait Objects

Trait objects are a generic way to access any type with a given trait, without
knowing all such possible types at compile time.
An example of a trait object is `Box<dyn Draw>`, a box with any type that
implements the `Draw` trait.

```rust
pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}
```

The example above (a *dynamic dispatch*) is more flexible than using generic
types (*static dispatch*), since one `Screen` instance can hold a vector with
different types in it.
However, there is additional overhead at runtime and less possible optimization
at compile time.

Only *object-safe* traits can be used for trait objects:
- No trait method can have the return type `Self`
- No generic type parameters can be used in the trait

Not following these rules will result in a compile time error.

### OO Design Patterns

The section gives an example of implementing the state pattern in Rust and what
could be simplified with more idiomatic design choices.


## 18. Patterns and Matching

Patterns are a Rust syntax for matching against the structure of types.
They can consist of:
- Literals
- Destructed arrays, enums, structs or tuples
- Variables
- Wildcards
- Placeholders

### All the Places for Patterns

- `match` arms (need to be *exhaustive*)
- `if let` (+`else`) expressions (match against a single pattern)
- `while let` (match one pattern as often as possible)
- `for p in x` (match pattern p against every item in x)
- `let` statements (compiler error if no match)
- functional parameters

### Refutability

Functional parameters, `let` statements and `for` loops can only accept
*irrefutable* patterns, i.e. patterns that match in any possible case.
`if let` and `while let` accept irrefutable and refutable patterns, but
generate a compiler warning against irrefutable patterns.
`match` arms *must* use refutable patterns, except for the last arm.

### Pattern Syntax

Valid syntax elements for patterns are:
- directly matching literals
- matching named variables (match any value irrefutable)
  *might shadow variables* of a surrounding scope
- one of multiple patterns in `match` with the or operator `|`
- an inclusive range of numeric or `char` values with `..=` (e.g. `'a'..='j'`)
- destructing structs, enums, tuples and references:
```rust
// destructing a tuple
let t = (0, 7);
let (a, b) = t;
assert_eq!(0, a);
assert_eq!(7, b);

// destructing a struct
let p = Point { x: 0, y: 7 };
let Point { x: a, y: b } = p;
assert_eq!(0, a);
assert_eq!(7, b);
// shorthand if the fields and variables have the same name:
let Point { x, y } = p;
assert_eq!(0, x);
assert_eq!(7, y);
// with literals in match:
match p {
    Point { x, y: 0 } => println!("On the x axis at {}", x),
    Point { x: 0, y } => println!("On the y axis at {}", y),
    Point { x, y } => println!("On neither axis: ({}, {})", x, y),
}

// destructing inner values of enums:
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}
let msg = Message::ChangeColor(0, 160, 255);
match msg {
    Message::Quit => // ..
    Message::Move { x, y } => // ..
    Message::Write(text) => // ..
    Message::ChangeColor(r, g, b) => // ..
}

// matching of these types can be nested:
let ((feet, inches), Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });
assert_eq!(3, feet);
assert_eq!(10, inches);
assert_eq!(3, x);
assert_eq!(-10, y);
```
- ignoring values completely with `_` (matches any value, but doesn't bind)
- not using a variable by beginning its name with `_` (still binds the value)
- ignoring parts of a value with `..` (must be unambiguous):
```rust
// with a struct
struct Point {
    x: i32,
    y: i32,
    z: i32,
}
let origin = Point { x: 0, y: 0, z: 0 };
match origin {
    Point { x, .. } => println!("x is {}", x),
}
// with a tuple
let numbers = (2, 4, 8, 16, 32);
match numbers {
    (first, .., last) => {
        println!("Some numbers: {}, {}", first, last);
    }
}
```
- extra conditionals with match guards (using `if`):
```rust
let num = Some(4);
match num {
    Some(x) if x < 5 => println!("less than five: {}", x),
    Some(x) => println!("{}", x),
    None => (),
}
```
- `@` bindings (for binding a value while also testing it for a pattern)
```rust
enum Message {
    Hello { id: i32 },
}
let msg = Message::Hello { id: 5 };
match msg {
    Message::Hello {
        id: id_variable @ 3..=7,
    } => println!("Found an id in range: {}", id_variable),
    Message::Hello { id: 10..=12 } => {
        println!("Found an id in another range")
    }
    Message::Hello { id } => println!("Found some other id: {}", id),
}
```


## 19. Advanced Features

### Unsafe Rust

Unsafe Rust disables some of the usual safety guarantees to switch to a manual
safety check for certain low-level tasks.

To use unsafe Rust, the `unsafe` keyword must be used before a new block.
The additional possibilities in unsafe Rust are:
- Dereferencing a raw pointer
- Calling an unsafe function or method
- Access or modify a mutable static variable
- Implement an unsafe trait
- Access fields of `union`s

#### Dereferencing a raw pointer
Raw pointers have the types `*const T` or `*mut T` and can be created from references:
```rust
let mut num = 5;
let r1 = &num as *const i32;
let r2 = &mut num as *mut i32;
```
Dereferencing them with the `*` operator requires an `unsafe` block.

#### Calling an unsafe function or method
Functions marked as `usafe fn` can only be called inside of `unsafe` blocks.
This is also true for `extern` functions, that stem from other languages:
```rust
extern "C" {
    fn abs(input: i32) -> i32;
}

fn main() {
    unsafe {
        println!("Absolute value of -3 according to C: {}", abs(-3));
    }
}
```

### Advanced Traits

### advanced types

### Advanced Functions and Closures

Functions can be passed to other functions as arguments of the type `fn`:

```rust
fn add_one(x: i32) -> i32 {
    x + 1
}
fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
    f(arg) + f(arg)
}
fn main() {
    let answer = do_twice(add_one, 5);
    println!("The answer is: {}", answer);
}
```

These *function pointers* implement all three closure traits `Fn`, `FnMut` and
`FnOnce`, so it's usually better to define a generic type with one of these
traits as an agument.

Closures can not be returned directy from a function, but as a trait object
it's possible:

```rust
fn returns_closure() -> Box<dyn Fn(i32) -> i32> {
    Box::new(|x| x + 1)
}
```

### Macros

A macro is code that writes other code.
Rust has *declarative macros* with `macro_rules!` and three kinds of
*procedual* macros:
- `#[derive]` macros
- Attribute-like macros
- Function-like macros

-- partially skipped --



## 20. final project a web server
### single threaded
### multithreaded
### graceful shutdown and cleanup
